(function() {
	'use strict';

	var patientControllers = angular.module('patientControllers', []);

	patientControllers.controller('allPatientsController', [ '$scope', 'mdPatient', allPatientsController]);
	patientControllers.controller('createPatientController', ['$scope', '$timeout', 'mdPatient', createPatientController]);
	patientControllers.controller('allProfilesController', ['$scope', '$routeParams', 'mdPatient', allProfilesController]);
	patientControllers.controller('updatePatientController', ['$scope', '$timeout', '$routeParams', 'mdPatient', updatePatientController]);
								   
	
	function allPatientsController($scope, Patient) {
		$scope.patients = Patient.$find();
	};

	function createPatientController($scope, $setTimeout, Patient){
		$scope.patient = {};

		$scope.doCreate = function() {
			var promise = Patient.$create($scope.patient);
			promise.then(function(data) {
				$timeout(function() {
					$scope.message = data.message;
				});
			});
		};
	};

	function allProfilesController($scope, $routeParams, Patient) {
		console.log($routeParams);
		$scope.patient = Patient.$find($routeParams.id);
	};

	function updatePatientController($scope, $setTimeout, $routeParams, Patient) {
		var patientId = $routeParams.id;
    	$scope.patient = Patient.$find(patientId);

			$scope.doUpdate = function() {
			var promise = Patient.$update($scope.patient, $routeParams.id);
			promise.then(function(data) {
				$timeout(function() {
					$scope.message = data.message;
				});
			});
		};

			// $scope.deletePatient = function(Patient){

   //   		if (confirm('Are you sure you want to delete this patient?')) {
   //      	$scope.returnedPatient = Patient.$deletePatient(patientId);
   //      	$scope.returnedPatient.then(function(data) {
   //      	alert('patient was deleted');
   //      	})
   //    		} else {
   //    		alert('patient was not deleted.');
   //  }   
   //  };
	};
	
})();