(function() {
	'use strict';

	function Patient(patientData) {

		if (!patientData.inspect) {
			_.extend(this, patientData);
			return;
		}
		this.$unwrap(patientData);
	}

	Patient.$factory = ['$timeout', '$location', 'mdResource',
		function($timeout, $location, Resource) {
			_.extend(Patient, {
				$timeout: $timeout,
				$location: $location,
				$$resource: new Resource('patients')
			});
			return Patient;
		}
	];

	angular.module('hipperModels').factory('mdPatient', Patient.$factory);

	Patient.$find = function(uid) {
		var patientData = this.$$resource.find(uid);

		if (uid) {
			var patient = new Patient(patientData);
			patient.id = uid;
			return patient;
		}


		return Patient.$unwrapCollection(patientData);
	};

	Patient.prototype.$unwrap = function(patientData) {
		var self = this;

		this.$patientData = patientData;
		this.$patientData.then(function(data) {
			Patient.$timeout(function() {
				_.extend(self, data);
			});
		});
	};

	Patient.$unwrapCollection = function(patientData) {
		var collection = {};
		collection.$patientData = patientData;
		collection.patients = [];

		patientData.then(function(patients) {
			Patient.$timeout(function() {
				
				_.reduce(patients, function(c, patient) {
					c.patients.push(new Patient(patient));
					return c;
				}, collection);
			});
		});
		
		return collection.patients;
	};

	Patient.$create = function(data) {
		var deffered = Q.defer();

		var promise = this.$$resource.create(data);
		promise
			.then(function(data) {
				Patient.$timeout(function() {
					console.log('DOING IT');
					Patient.$location.path('/patients');
				});
			})
			.fail(function(data) {
				deffered.resolve(data);
			});

		return deffered.promise;
	};

	// Patient.prototype.editPatient = function() {
	// 	var self = this;
	// 	var id = self.id;
	// 	var path = '/patients/' + id + '/edit';
	// 	console.log(path);

	// 	Patient.$location.path(path);
	// };

	Patient.$update = function(data, pathId) {
		var deffered = Q.defer();

		var promise = this.$$resource.update(data, pathId);
		promise
			.then(function(data) {
				Patient.$timeout(function() {
					console.log('updating patient id=' + pathId);
					Patient.$location.path('/patients/profile/' + pathId);
				});
			})
			.fail(function(data) {
				deffered.resolve(data);
			});

		return deffered.promise;
	};
})();