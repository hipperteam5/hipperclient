(function() {
	'use strict';

	function Admin(adminData) {

		if (!adminData.inspect) {
			_.extend(this, adminData);
			return;
		}
		this.$unwrap(adminData);
	}

	Admin.$factory = ['$timeout', '$location', 'mdResource',
		function($timeout, $location, Resource) {
			_.extend(Admin, {
				$timeout: $timeout,
				$location: $location,
				$$resource: new Resource('admins')
			});
			return Admin;
		}
	];

	angular.module('hipperModels').factory('mdAdmin', Admin.$factory);

	Admin.$find = function(uid) {
		var adminData = this.$$resource.find(uid);

		if (uid) {
			var admin = new Admin(adminData);
			admin.id = uid;
			return admin;
		}

		return Admin.$unwrapCollection(adminData);
	};

	Admin.prototype.$unwrap = function(adminData) {
		var self = this;

		this.$adminData = adminData;
		this.$adminData.then(function(data) {
			Admin.$timeout(function() {
				_.extend(self, data);
			});
		});
	};

	Admin.$unwrapCollection = function(adminData) {
		var collection = {};
		collection.$adminData = adminData;
		collection.admins = [];

		adminData.then(function(admins) {
			Admin.$timeout(function() {
				_.reduce(admins, function(c, admin) {
					c.admins.push(new Admin(admin));
					return c;
				}, collection);
			});
		});

		return collection.admins;
	};

	Admin.$create = function(data) {
		var deffered = Q.defer();

		var promise = this.$$resource.create(data);
		promise
			.then(function(data) {
				Admin.$timeout(function() {
					console.log('DOING IT');
					Admin.$location.path('/admins');
				});
			})
			.fail(function(data) {
				deffered.resolve(data);
			});

		return deffered.promise;
	};

	Admin.prototype.editAdmin = function() {
		var self = this;
		var id = self.id;
		var path = '/admins/' + id + '/edit';
		console.log(path);

		Admin.$location.path(path);
	};
})();