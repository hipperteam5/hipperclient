(function() {
	'use strict';

	var adminControllers = angular.module('adminControllers', []);

	adminControllers.controller('allAdminsController', ['$scope', 'mdAdmin', allAdminsController]);
	adminControllers.controller('createAdminController', ['$scope', '$timeout', 'mdAdmin', createAdminController]);
	adminControllers.controller('navAdminController', ['$scope', '$location', navAdminController]);

	function allAdminsController($scope, Admin) {
		$scope.admins = Admin.$find();
	};		

	function createAdminController($scope, $timeout, Admin) {
		$scope.admin = {};

		$scope.doCreate = function () {
			var promise = Admin.$create($scope.admin);
			promise.then(function(data) {
				$timeout(function() {
					$scope.message = data.message;
				});
			});
		};
	};

	function navAdminController($scope, $location) {
		$scope.isActive = function (viewLocation) {
			return viewLocation === $location.path();
		};
	};

})();