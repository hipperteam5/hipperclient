(function() {

	'use strict';

	var hipperControllers = angular.module('hipperControllers', []);

	hipperControllers.controller('menuController', ['$scope', 'auth', '$location', '$timeout', menuController]);
	hipperControllers.controller('homeController', ['$scope', 'auth', homeController]);
	hipperControllers.controller('signInController', ['$scope', 'auth', signInController]);

	function homeController($scope, auth) {
		$scope.$watch(auth.isLoggedIn, function(isLoggedIn) {
			$scope.status = isLoggedIn;
			$scope.currentUser = auth.currentUser();
			$scope.isAdmin = auth.isAdmin();
			$scope.isTherapist = auth.isTherapist();
		});
	};

	function menuController($scope, auth, $location, $timeout) {
		$scope.status = false;
		$scope.$watch(auth.isLoggedIn, function(isLoggedIn) {
			$scope.status = isLoggedIn;
			$scope.currentUser = auth.currentUser();
			$scope.isAdmin = auth.isAdmin();
			$scope.isTherapist = auth.isTherapist();
			console.log(auth.isTherapist());
		});

		$scope.goRestricted = function() {
			console.log('GO');
			$location.path('/restricted');
		};

		$scope.goTherapists = function() {
			console.log('GO');
			$location.path('/therapists');
		};

		$scope.goPatients = function() {
			console.log('GO');
			$location.path('/patients');
		};

		$scope.goExercises = function() {
			console.log('GO');
			$location.path('/exercises');
		};

		$scope.goCreateExercise = function() {
			console.log('CREATE EXERCISE');
			$location.path('/exercises/create');
		};

		$scope.goLineGraph = function() {
			console.log('GO');
			$location.path('/graph');
		};

		$scope.goCreatePatient = function() {
			console.log('CREATE PATIENT');
			$location.path('/patients/create');
		}

		$scope.goUpdatePatient = function(patientId) {
			console.log('GoUpdate' + patientId);
			$location.path('/patients/update/' + patientId);
		};


		$scope.goAdmins = function() {
			console.log('ADMINS');
			$location.path('/admins');
		};

		$scope.goCreateAdmin = function() {
			console.log('CREATE ADMIN');
			$location.path('/admins/create');
		};

		$scope.goCreateTherapists = function() {
			console.log('CREATE THERAPIST');
			$location.path('/therapists/create');

		};

		$scope.goResultAgenda = function(patientId) {
			console.log('GO');
			$location.path('/patients/' + patientId + '/results');
		};

		$scope.goAgenda = function(patientId) {
			console.log('GO');
			$location.path('/agenda/' + patientId);
		};

		$scope.goProfile = function(patientId) {
			console.log('Go' + patientId);
			$location.path('/patients/profile/' + patientId);
		};
		$scope.goExerciseProfile = function(exerciseId) {
			console.log('Go' + exerciseId);
			$location.path('/exercises/view/' + exerciseId);
		};
		$scope.goUpdateExercise = function(exerciseId) {
			console.log('GoUpdateExerciseId=' + exerciseId);
			$location.path('/exercises/update/' + exerciseId);
		};

		$scope.doSignOut = function() {
			auth.logout();
		};
	};

	function signInController($scope, auth) {
		$scope.credentials = {
			role: 2
		};

		$scope.admin = function() {
			if ($scope.credentials.role === 1) return true;
			return false;
		};
		console.log($scope.admin());

		$scope.doSignIn = function() {
			console.log($scope.credentials);
			auth.login($scope.credentials, function(message) {
				$scope.message = message;
			});
		};
	}

})();