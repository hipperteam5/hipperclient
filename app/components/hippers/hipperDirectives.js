(function() {
	var hipperDirectives = angular.module('hipperDirectives', []);


	hipperDirectives.directive('menuHipper', function() {
		return {
			restrict: 'E',
			templateUrl: 'partials/app/menu.html',
			controller: 'menuController'
		};
	});

	hipperDirectives.directive('therapistMenu', function() {
		return {
			restrict: 'E',
			templateUrl: 'partials/app/therapistMenu.html',
			controller: 'navTherapistController'
		};
	});
	hipperDirectives.directive('adminMenu', function() {
		return {
			restrict: 'E',
			templateUrl: 'partials/app/adminMenu.html',
			controller: 'navAdminController'
		};
	});
	hipperDirectives.directive('therapistPatients', function() {
		return {
			restrict: 'E',
			templateUrl: 'partials/patients/directive.html',
			controller: 'allPatientsController'
		};
	});
	hipperDirectives.directive('allExercises', function() {
		return {
			restrict: 'E',
			templateUrl: 'partials/exercises/directive.html',
			controller: 'allExerciseController'
		};
	});

})();