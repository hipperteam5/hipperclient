(function() {

	'use strict';

	var hipperFactories = angular.module('hipperFactories', []);

	hipperFactories.factory('api', [api]);
	hipperFactories.factory('auth', ['$http', '$location', 'api', auth]);

	function api() {
	 // DEVELOPMENT  	
	 // var server = "http://localhost:8080/"; 
	 // PRODUCTION // 
	 
	 var server = "http://015.006.092.145.hva.nl:8080/";
 	
		var resources = "secured/";

		return {
			getServer: function() {
				return server;
			},

			getResources: function() {
				return server + resources;
			}
		};
	}


	function auth($http, $location, api) {
		var currentUser;
		var authStatus = false;
		var role = null;
		
		var loginByToken = function() {
			$http.get(api.getResources() + 'me')
				.success(function(data, status) {
					authStatus = true;
					currentUser = data;
					/*
					Role config
					*/
					role = currentUser.roles[0];
				})
				.error(function(data, status, headers, config) {
					localStorage.removeItem("hipperaccesstoken");
					currentUser = null;
					role = null;
					authStatus = false;
					$location.path('/signin');
				});
		};

		if (localStorage.getItem("hipperaccesstoken")) {
			console.log('Token already there');
			loginByToken();
		} else {
			console.log('No Token Found, Sign in');
			
			$location.path('/signin');
		}

		return {
			login: function(credentials, callback) {
				var self = this;
				console.log(credentials);
				$http.post(api.getServer() + 'authenticate', credentials)
					.success(function(data, status) {
						self.setCurrent(data.user);
						authStatus = true;
						localStorage.setItem("hipperaccesstoken", data.hipperAccessToken);
						$location.path('/');
					})
					.error(function(data, status, headers, config) {
						currentUser = null;
						authStatus = false;
						console.log(status);
						console.log(config);
						localStorage.removeItem("hipperaccesstoken");
						callback("Invalid credentials");
					});
			},
			logout: function() {
				authStatus = false;
				currentUser = null;
				localStorage.removeItem("hipperaccesstoken");
				$location.path('/signin');
			},
			isLoggedIn: function() {
				return authStatus;
			},
			currentUser: function() {
				return currentUser;
			},
			currentRole: function() {
				return role;
			},
			setNextPath: function() {
				nextPath = path;
			},
			getNextPath: function() {
				return nextPath;
			},
			isAdmin: function() {
				return role === "ADMIN";
			},
			isTherapist: function() {
				return role === "THERAPIST";
			},
			setCurrent: function(userData) {
				currentUser = userData;
				role = currentUser.roles[0];
				authStatus = true;
			}
		};
	}


})();
