	(function() {
		'use strict';

		function Weekresult(weekresultData) {
			if (!weekresultData.inspect) {
				_.extend(this, weekresultData);
				return;
			}
			this.$unwrap(weekresultData);
		}

		Weekresult.$factory = ['$timeout', '$location', 'mdResource',
			function($timeout, $location, Resource) {
				_.extend(Weekresult, {
					$timeout: $timeout,
					$location: $location,
					$$resource: new Resource('weekresults')
				});
				return Weekresult;
			}
		];

		angular.module('hipperModels').factory('mdWeekresult', Weekresult.$factory);

		Weekresult.$find = function(uid, weeknumber) {
			var weekresultData = this.$$resource.getByTwoParams(uid, weeknumber);
			
			if (uid && weeknumber) {
				var weekresult = new Weekresult(weekresultData);
				return weekresult;
			} else {
				return null;
			}
		};

		Weekresult.prototype.$unwrap = function(weekresultData) {
			var self = this;

			this.$weekresultData = weekresultData;
			this.$weekresultData.then(function(data) {
				Weekresult.$timeout(function() {
					_.extend(self, data);
				});
			});
		};	


	})();