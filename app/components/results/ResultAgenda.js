(function() {
	'use strict';

	function ResultAgenda(resultAgendaData) {
		if (!resultAgendaData.inspect) {
			_.extend(this, resultAgendaData);
			return;
		}
		this.$unwrap(resultAgendaData);
	}

	ResultAgenda.$factory = ['$timeout', '$location', 'mdResource',
		function($timeout, $location, Resource) {
			_.extend(ResultAgenda, {
				$timeout: $timeout,
				$location: $location,
				$$resource: new Resource('resultsagendas')
			});
			return ResultAgenda;
		}
	];

	angular.module('hipperModels').factory('mdResultAgenda', ResultAgenda.$factory);

	ResultAgenda.$find = function(uid) {
		var resultAgendaData = this.$$resource.find(uid);

		if (uid) {
			var resultAgenda = new ResultAgenda(resultAgendaData);
			resultAgenda.id = uid;
			return resultAgenda;
		}

		return ResultAgenda.$unwrapCollection(resultAgendaData);
	};

	ResultAgenda.prototype.$unwrap = function(resultAgendaData) {
		var self = this;

		this.$resultAgendaData = resultAgendaData;
		this.$resultAgendaData.then(function(data) {
			ResultAgenda.$timeout(function() {
				_.extend(self, data);
			});
		});
	};

	ResultAgenda.$unwrapCollection = function(resultAgendaData) {
		var collection = {};
		collection.$resultAgendaData = resultAgendaData;
		collection.ResultAgendas = [];

		resultAgendaData.then(function(resultAgenda) {
			ResultAgenda.$timeout(function() {
				_.reduce(resultAgendas, function(c, resultAgendas) {
					c.resultAgendas.push(new ResultAgenda(resultAgendas));
					return c;
				}, collection);
			});
		});

		return collection.ResultAgendas;
	};

})();