(function() {
  'use strict';

  var resultAgendaControllers = angular.module('resultAgendaControllers', ['ui.calendar']);
  resultAgendaControllers.controller('resultAgendaController', ['$scope', 'mdResultAgenda', 'mdPatient', '$timeout',
    'uiCalendarConfig', '$routeParams', '$location', resultAgendaController
  ]);

  function resultAgendaController($scope, ResultAgenda, Patient, $timeout, uiCalendarConfig, $routeParams, $location) {
    var patientId = $routeParams.id
    $scope.patient = Patient.$find(patientId);
    $scope.resultAgendas = ResultAgenda.$find(patientId);

    $scope.resultAgendas.$resultAgendaData.then(function(data) {
      $timeout(function() {
        $scope.result = data;
        $scope.changeView('basicWeek', 'myCalendar');
        $scope.changeView('month', 'myCalendar');
      });
    });


    $scope.changeView = function(view, calendar) {
      
      uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
    };

    $scope.dayClick = function(date, allDay, jsEvent, view) {
      var year = 2014;
      $location.path('/patients/' + patientId  + '/results/year/' + year +'/week/'  + date.week());
    };

    $scope.dayRender = function(date, cell) {
      if ($scope.result) {
        var today = date.format();
        for (var i = 0; i < $scope.result.dailyAverageResults.length; i++) {
          if (today === $scope.result.dailyAverageResults[i].date && $scope.result.dailyAverageResults[i].resultColor === 1) {
            cell.css("background-color", "green");
          } else if (today === $scope.result.dailyAverageResults[i].date && $scope.result.dailyAverageResults[i].resultColor === 2) {
            cell.css("background-color", "yellow");
          } else if (today === $scope.result.dailyAverageResults[i].date && $scope.result.dailyAverageResults[i].resultColor === 3) {
            cell.css("background-color", "red");
          }
        }
      }
    };

    $scope.uiConfig = {
      calendar: {
        height: 450,
        editable: true,
        firstDay: 1,
        weekNumbers: true,
        header: {
          left: 'basicWeek month',
          center: 'title',
          right: 'today prev,next'
        },
        dayClick: $scope.dayClick,
        eventDrop: $scope.alertOnDrop,
        eventResize: $scope.alertOnResize,
        eventClick: $scope.eventClick,
        viewRender: $scope.renderView,
        dayRender: $scope.dayRender
      }
    };
    $scope.events = [];
    $scope.eventSources = [$scope.events];
  };
})();