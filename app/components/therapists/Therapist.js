(function() {
	'use strict';

	function Therapist(therapistData) {

		if (!therapistData.inspect) {
			_.extend(this, therapistData);
			return;
		}
		this.$unwrap(therapistData);
	}

	Therapist.$factory = ['$timeout', '$location', 'mdResource',
		function($timeout, $location, Resource) {
			_.extend(Therapist, {
				$timeout: $timeout,
				$location: $location,
				$$resource: new Resource('therapists')
			});
			return Therapist;
		}
	];

	angular.module('hipperModels').factory('mdTherapist', Therapist.$factory);

	Therapist.$find = function(uid) {
		var therapistData = this.$$resource.find(uid);

		if (uid) {
			var therapist = new Therapist(therapistData);
			therapist.id = uid;
			return therapist;
		}

		return Therapist.$unwrapCollection(therapistData);
	};

	Therapist.prototype.$unwrap = function(therapistData) {
		var self = this;

		this.$therapistData = therapistData;
		this.$therapistData.then(function(data) {
			Therapist.$timeout(function() {
				_.extend(self, data);
			});
		});
	};

	Therapist.$unwrapCollection = function(therapistData) {
		var collection = {};
		collection.$therapistData = therapistData;
		collection.therapists = [];

		therapistData.then(function(therapists) {
			Therapist.$timeout(function() {
				_.reduce(therapists, function(c, therapist) {
					c.therapists.push(new Therapist(therapist));
					return c;
				}, collection);
			});
		});

		return collection.therapists;
	};

	Therapist.$create = function(therapist) {
		Therapist.$location.path('/therapists');
		return this.$$resource.create(therapist);
	};

	Therapist.prototype.viewProfile = function() {
		var self = this;
		var id = self.id;
		var path = '/therapists/';
		console.log(path);
		Therapist.$location.path(path);
	};




})();