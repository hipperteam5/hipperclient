(function() {
	'use strict';

	var therapistControllers = angular.module('therapistControllers', []);

	therapistControllers.controller('allTherapistsController', ['$scope', 'mdTherapist', allTherapistsController]);
	therapistControllers.controller('createTherapistsController', ['$scope', '$translate', 'mdTherapist', createTherapistsController]);
	therapistControllers.controller('navTherapistController', ['$scope', '$location', navTherapistController]);

	function allTherapistsController($scope, Therapist) {
		$scope.therapists = Therapist.$find();
	};

	function createTherapistsController($scope, $translate, Therapist) {
		$scope.therapist = {};

		$scope.doCreate = function() {
			Therapist.$create($scope.therapist);
		};
		$scope.changeLanguage = function(langKey) {
			$translate.use(langKey);
		};

	};

	function navTherapistController($scope, $location) {
		$scope.isActive = function(viewLocation) {
			return viewLocation === $location.path();
		};
	};

})();