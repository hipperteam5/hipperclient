(function() {
	'use strict';

	var exerciseControllers = angular.module('exerciseControllers', []);

	exerciseControllers.controller('allExerciseController', ['$scope', 'mdExercise', allExerciseController]);
	exerciseControllers.controller('createExerciseController', ['$scope', 'mdExercise', createExerciseController]);
	exerciseControllers.controller('exerciseViewController', ['$scope', '$routeParams', 'mdExercise', exerciseViewController]);
	exerciseControllers.controller('updateExerciseController', ['$scope', '$timeout', '$routeParams', 'mdExercise', updateExerciseController]);

	function allExerciseController($scope, Exercise) {
		$scope.exercises = Exercise.$find();
	};

	function createExerciseController($scope, Exercise) {
		$scope.exercise = {};

		$scope.doCreate = function() {
			Exercise.$create($scope.exercise);
		};
	};

	function exerciseViewController($scope, $routeParams, Exercise) {
		console.log($routeParams);
		$scope.exercise = Exercise.$find($routeParams.id);
	};

	function updateExerciseController($scope, $setTimeout, $routeParams, Exercise) {
		var exerciseId = $routeParams.id;
    	$scope.exercise = Exercise.$find(exerciseId);

			$scope.doUpdate = function() {
			var promise = Exercise.$update($scope.exercise, $routeParams.id);
			promise.then(function(data) {
				$timeout(function() {
					$scope.message = data.message;
				});
			});
		};

	};

})();