	(function() {
		'use strict';

		function Weekresult(weekresultData) {
			if (!weekresultData.inspect) {
				_.extend(this, weekresultData);
				return;
			}
			this.$unwrap(weekresultData);
			this.showPleaseWait();
		}

		Weekresult.$factory = ['$timeout', '$location', 'mdResource',
			function($timeout, $location, Resource) {
				_.extend(Weekresult, {
					$timeout: $timeout,
					$location: $location,
					$$resource: new Resource('weekresults')
				});
				return Weekresult;
			}
		];

		angular.module('hipperModels').factory('mdWeekresult', Weekresult.$factory);

		Weekresult.$find = function(uid, weeknumber) {
			var weekresultData = this.$$resource.getByTwoParams(uid, weeknumber);
			
			if (uid && weeknumber) {
				var weekresult = new Weekresult(weekresultData);
				return weekresult;
			} else {
				return null;
			}
		
		};

		Weekresult.prototype.$unwrap = function(weekresultData) {
			var self = this;

			this.$weekresultData = weekresultData;
			this.$weekresultData.then(function(data) {
				Weekresult.$timeout(function() {
					_.extend(self, data);
				});
			});
		};	

// trying to make a loadingscreen, graphs take some time to load

    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    return {
        showPleaseWait: function() {
            pleaseWaitDiv.modal();
        },
        hidePleaseWait: function () {
            pleaseWaitDiv.modal('hide');
        },

    };


	})();