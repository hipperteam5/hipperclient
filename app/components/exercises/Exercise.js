(function() {
	'use strict';

	function Exercise(exerciseData) {

		if (!exerciseData.inspect) {
			_.extend(this, exerciseData);
			return;
		}
		this.$unwrap(exerciseData);
	}

	Exercise.$factory = ['$timeout', '$location', 'mdResource',
		function($timeout, $location, Resource) {
			_.extend(Exercise, {
				$timeout: $timeout,
				$location: $location,
				$$resource: new Resource('exercises')
			});
			return Exercise;
		}
	];

	angular.module('hipperModels').factory('mdExercise', Exercise.$factory);

	Exercise.$find = function(uid) {
		var exerciseData = this.$$resource.find(uid);

		if (uid) {
			var exercise = new Exercise(exerciseData);
			exercise.id = uid;
			return exercise;
		}

		return Exercise.$unwrapCollection(exerciseData);
	};

	Exercise.prototype.$unwrap = function(exerciseData) {
		var self = this;

		this.$exerciseData = exerciseData;
		this.$exerciseData.then(function(data) {
			Exercise.$timeout(function() {
				_.extend(self, data);
			});
		});
	};

	Exercise.$unwrapCollection = function(exerciseData) {
		var collection = {};
		collection.$exerciseData = exerciseData;
		collection.exercises = [];

		exerciseData.then(function(exercises) {
			Exercise.$timeout(function() {
				_.reduce(exercises, function(c, exercise) {
					c.exercises.push(new Exercise(exercise));
					return c;
				}, collection);
			});
		});

		return collection.exercises;
	};

	Exercise.$create = function(exercise) {
		var deffered = Q.defer();

		var promise = this.$$resource.create(exercise);
		promise
			.then(function(data) {
				Exercise.$timeout(function() {
					console.log('DOING IT');
					Exercise.$location.path('/exercises');
				});
			})
			.fail(function(data) {
				deffered.resolve(data);
			});
		return deffered.promise;



	};

	Exercise.prototype.viewProfile = function() {
		var self = this;
		var id = self.id;
		var path = '/exercises/';
		console.log(path);
		Exercise.$location.path(path);
	};

	Exercise.$update = function(data, pathId) {
		var deffered = Q.defer();

		var promise = this.$$resource.update(data, pathId);
		promise
			.then(function(data) {
				Exercise.$timeout(function() {
					console.log('updating exercise id=' + pathId);
					Exercise.$location.path('/exercises/view/' + pathId);
				});
			})
			.fail(function(data) {
				deffered.resolve(data);
			});

		return deffered.promise;
	};

	
})();