	(function() {
		'use strict';

		function Agenda(agendaData) {
			if (!agendaData.inspect) {
				_.extend(this, agendaData);
				return;
			}
			this.$unwrap(agendaData);
		}

		Agenda.$factory = ['$timeout', '$location', 'mdResource',
			function($timeout, $location, Resource) {
				_.extend(Agenda, {
					$timeout: $timeout,
					$location: $location,
					$$resource: new Resource('agendas')
				});
				return Agenda;
			}
		];

		angular.module('hipperModels').factory('mdAgenda', Agenda.$factory);

		Agenda.$find = function(uid) {
			var agendaData = this.$$resource.find(uid);

			if (uid) {
				var agenda = new Agenda(agendaData);
				agenda.id = uid;
				return agenda;
			}

			return Agenda.$unwrapCollection(agendaData);
		};

		Agenda.prototype.$unwrap = function(agendaData) {
			var self = this;

			this.$agendaData = agendaData;
			this.$agendaData.then(function(data) {
				Agenda.$timeout(function() {
					_.extend(self, data);
				});
			});
		};

		Agenda.$unwrapCollection = function(agendaData) {
			var collection = {};
			collection.$agendaData = agendaData;
			collection.agendas = [];

			agendaData.then(function(agenda) {
				Agenda.$timeout(function() {
					_.reduce(agendas, function(c, agendas) {
						c.agendas.push(new Agenda(agendas));
						return c;
					}, collection);
				});
			});

			return collection.agendas;
		};

		Agenda.$add = function(data, id) {
			return this.$$resource.addById(data, id);
		};

		Agenda.$deleteEvent = function(id, assignedExerciseId){
			return this.$$resource.removeExercise(id, assignedExerciseId);
		};

	})();