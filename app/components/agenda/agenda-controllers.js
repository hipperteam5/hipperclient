(function() {
  'use strict';

  var agendaControllers = angular.module('agendaControllers', ['ui.calendar', 'ngDragDrop']);

  agendaControllers.controller('agendaController', ['$scope', 'mdAgenda', 'mdPatient', 'mdExercise', 'uiCalendarConfig',
    '$routeParams', agendaController
  ]);

  function agendaController($scope, Agenda, Patient, Exercise, uiCalendarConfig, $routeParams) {
    var patientId = $routeParams.id;
    $scope.patient = Patient.$find(patientId);
    $scope.agendas = Agenda.$find(patientId);
    $scope.exercises = Exercise.$find();

    //repetition input checkbox
    $scope.checkboxValue = false;

    $scope.agendas.$agendaData.then(function(data) {
      $scope.events = data.events;
      $scope.eventSources.push($scope.events);
    });


    $scope.getExerciseData = function(event, ui, exercise) {
      //function called on dragging exercise
      $scope.exerciseTitle = exercise.exerciseName;
      $scope.exerciseId = exercise.id;
      $scope.exerciseReps = exercise.repetitions;
    };

    $scope.rerenderEvents = function(current) {
      uiCalendarConfig.calendars['myCalendar'].fullCalendar('removeEventSource', $scope.events);
      $scope.events = current;
      uiCalendarConfig.calendars['myCalendar'].fullCalendar('addEventSource', $scope.events);
    }

    $scope.getSelectedEvent = function() { //function called on select-box drop
      $scope.selectedTitle = $scope.exerciseTitle;
      $scope.selectedId = $scope.exerciseId;
      $scope.selectedReps = $scope.exerciseReps;

      if ($scope.checkboxValue == true) {
        do {
          $scope.selectedReps = parseInt(prompt("How many reps? (enter a number)"));
        } while (isNaN($scope.exerciseReps));
      }
    };

    $scope.clearSelected = function() {
      $scope.selectedTitle = undefined;
    };

    $scope.drop = function(date, jsEvent, ui, event, exercise) { //function called by dropping exercise on agenda
      var start = date.format();
      var eventList = [];

      if ($scope.checkboxValue == true) {
        do {
          $scope.exerciseReps = parseInt(prompt("How many reps? (enter a number)"));
        } while (isNaN($scope.exerciseReps));
      }

      var event = {
        exerciseId: $scope.exerciseId,
        start: start,
        repetitions: $scope.exerciseReps
      }
      eventList.push(event);
      $scope.postedAgenda = Agenda.$add(eventList, patientId);
      $scope.postedAgenda.then(function(data) {
        $scope.rerenderEvents(data.events);
      });
    };

    $scope.unselect = function() {
      uiCalendarConfig.calendars['myCalendar'].fullCalendar('unselect');
    };

    $scope.startSelect = function(start, end) { //function called by selecting multiple days
      var difference = end.diff(start, 'days');
      var eventList = [];
      var originalStart = start.format();

      if ($scope.selectedTitle == undefined) {
        $scope.unselect();
        alert("No exercise in selectable area, please drag an exercise to the selectable area");
      } else {
        for (var i = 0; i < difference; i++) {
          var dayString = start.format();

          var event = {
            exerciseId: $scope.selectedId,
            start: dayString,
            repetitions: $scope.selectedReps
          }
          eventList.push(event);
          start.add(1, 'd');
        }

        $scope.postedAgenda = Agenda.$add(eventList, patientId);
        $scope.postedAgenda.then(function(data) {
          $scope.rerenderEvents(data.events);
        });
        $scope.unselect();
        end.subtract(1, 'd');
        alert(difference + " exercises added from " + originalStart + " to " + end.format() + ".");
      }
    };

    $scope.deleteEvent = function(event, ui) {

      if (confirm('Are you sure you want to delete this event?')) {
        $scope.returnedAgenda = Agenda.$deleteEvent(patientId, event.assignedExerciseId);
        $scope.returnedAgenda.then(function(data) {
          $scope.rerenderEvents(data.events);
        })
      }
    };

    $scope.uiConfig = {
      calendar: {
        height: 450,
        editable: false,
        header: {
          left: 'month basicWeek',
          center: 'title',
          right: 'today prev,next'
        },

        droppable: true,
        selectable: true,
        firstDay: 1,
        dayClick: $scope.dayClick,
        eventDrop: $scope.alertOnDrop,
        drop: $scope.drop,
        select: $scope.startSelect,
        eventResize: $scope.alertOnResize,
        eventClick: $scope.deleteEvent,
        viewRender: $scope.renderView,
        dayRender: $scope.dayRender
      }
    };
    $scope.events = [];
    $scope.eventSources = [$scope.events];
  };
})();