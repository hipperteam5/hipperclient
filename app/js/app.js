(function() {
	'use strict';

	var hipper = angular.module('hipper', [
		'ngRoute',
		'hipperControllers',
		'hipperDirectives',
		'hipperFactories',
		'hipperModels',
		'therapistControllers',
		'graphControllers',
		'adminControllers',
		'patientControllers',
		'exerciseControllers',
		'resultAgendaControllers',
		'agendaControllers',
		'resultAgendaControllers',
		'pascalprecht.translate'
	]);



	hipper.config(['$routeProvider',


		function($routeProvider) {
			/*
				Authentication routes
			*/
			$routeProvider.when('/signin', {
				templateUrl: 'partials/authentication/signin.html',
				controller: 'signInController'
			});

			/*
				Standard routes
			*/
			$routeProvider.when('/', {
				templateUrl: 'partials/home.html',
				controller: 'homeController',
				requireLogin: true
			});

			/*
				Therapist Routes
			*/

			$routeProvider.when('/therapists', {
				templateUrl: 'partials/therapists/all.html',
				controller: 'allTherapistsController',
				requireLogin: true,
				requireAdmin: true
			});


			$routeProvider.when('/exercises/create', {
				templateUrl: 'partials/exercises/create.html',
				controller: 'createExerciseController',
				requireLogin: true,
				requireTherapist: true
			});

			$routeProvider.when('/exercises', {
				templateUrl: 'partials/exercises/all.html',
				controller: 'allExerciseController',
				requireLogin: true,
				requireTherapist: true
			});

			//           not used yet
			// $routeProvider.when('/exercises/details/:id', {
			// 	templateUrl: 'partials/exercises/detailGraph.html.html',
			// 	controller: 'detailedExerciseController',
			// 	requireLogin: true,
			// 	requireTherapist: true
			// });


			$routeProvider.when('/results/details', {
				templateUrl: 'partials/results/detailGraph.html',
				controller: 'detailedGraphController',
				requireLogin: true,
				requireTherapist: true
			});

			$routeProvider.when('/patients/:id/results/year/:year/week/:week', {
				templateUrl: 'partials/results/weekGraph.html',
				controller: 'barGraphController',
				requireLogin: true,
				requireTherapist: true
			});

			//TODO

			/*
				Patient Routes
			*/

			$routeProvider.when('/patients/:id/results', {
				templateUrl: 'partials/results/resultAgenda.html',
				controller: 'resultAgendaController',
				requireLogin: true,
				requireTherapist: true
			});

			$routeProvider.when('/agenda/:id', {
				templateUrl: 'partials/agenda/agenda.html',
				controller: 'agendaController',
				requireLogin: true,
				requireTherapist: true
			});

			$routeProvider.when('/patients', {
				templateUrl: 'partials/patients/all.html',
				controller: 'allPatientsController',
				requireLogin: true,

			});

			$routeProvider.when('/patients/create', {
				templateUrl: 'partials/patients/create.html',
				controller: 'createPatientController',
				requireLogin: true,
				requireTherapist: true

			});

			/* 
				Admin Routes
			*/


			$routeProvider.when('/admins', {
				templateUrl: 'partials/admins/all.html',
				controller: 'allAdminsController',
				requireLogin: true,
				requireAdmin: true
			});

			// <<<<<<< HEAD
			// 			$routeProvider.when('/patients/:id', {
			// =======
			$routeProvider.when('/patients/update/:id', {
				templateUrl: 'partials/patients/update.html',
				controller: 'updatePatientController',
				requireLogin: true,
				requireTherapist: true
			});

			$routeProvider.when('/patients/profile/:id', {
				// >>>>>>> validations
				templateUrl: 'partials/patients/profile.html',
				controller: 'allProfilesController',
				requireLogin: true,
				requireTherapist: true
			});

			$routeProvider.when('/exercises/view/:id', {
				templateUrl: 'partials/exercises/view.html',
				controller: 'exerciseViewController',
				requireLogin: true,
				requireTherapist: true
			});
			$routeProvider.when('/exercises/update/:id', {
				templateUrl: 'partials/exercises/update.html',
				controller: 'updateExerciseController',
				requireLogin: true,
				requireTherapist: true
			});

			$routeProvider.when('/admins/create', {
				templateUrl: 'partials/admins/create.html',
				controller: 'createAdminController',
				requireLogin: true,
				requireAdmin: true
			});

			$routeProvider.when('/therapists/create', {
				templateUrl: 'partials/therapists/create.html',
				controller: 'createTherapistsController',
				requireLogin: true,
				requireAdmin: true
			});


			/*
			Restricted to certain role example
			*/
			$routeProvider.when('/restricted', {
				templateUrl: 'partials/authentication/restricted.html',
				requireLogin: true,
				requireTherapist: true
			});

			/*
				Error Handling Routes
			*/
			$routeProvider.when('/403', {
				templateUrl: 'partials/error/403.html',
				requireLogin: true
			});
		}
	]).run(function($rootScope, $location, auth) {
		$rootScope.$on('$routeChangeStart', function(event, next, current) {

			if (current !== undefined) {

				// Check login
				if (next.requireLogin && !auth.isLoggedIn()) {
					event.preventDefault();
					console.log('Requires login!');
					return $location.path('/signin');
				}

				//Check for admin rights
				if (next.requireAdmin && !auth.isAdmin()) {
					console.log('ADMIN CHECK FAILED');
					event.preventDefault();
					console.log('Requires Administrator!');
					return $location.path('/403');
				}

				//Check for therapist rights
				if (next.requireTherapist && !auth.isTherapist()) {
					event.preventDefault();
					console.log('Requires Therapist!');
					return $location.path('/403');
				}
			}
		});
	});

	hipper.factory('authInterceptor', function($rootScope, $q, $location) {
		return {
			request: function(config) {
				config.headers = config.headers || {};
				if (localStorage.getItem("hipperaccesstoken")) {
					var token = localStorage.getItem("hipperaccesstoken");
					config.headers.hipperaccesstoken = token;
				}
				return config;
			},
			response: function(response) {
				if (response.status === 401) {
					$location.path('/signin');
				}
				return response || $q.when(response);
			}
		};
	});

	hipper.config(function($httpProvider) {
		$httpProvider.interceptors.push('authInterceptor');
	});

	/*
		Translation languages
	*/

	hipper.config(function($translateProvider) {
		var translationsEN= {
			TYPE: 'Therapist Type',
			PHONE: 'Phone Number',
			LASTNAME: 'Last Name',
			FIRSTNAME:  'First Name',
			USERNAME: 'Username',
			PASSWORD: 'Password',
			BUTTON_LANG_NL: 'dutch',
			BUTTON_LANG_EN: 'english'
		};
		var translationsNL= {
			TYPE: 'Therapeut Type',
			PHONE: 'Telefoon Nummer',
			LASTNAME: 'Voornaam',
			FIRSTNAME:  'Achternaam',
			USERNAME: 'Gebruikersnaam',
			PASSWORD: 'Wachtwoord',
			BUTTON_LANG_NL: 'nederlands',
			BUTTON_LANG_EN: 'engels'
		};		


		$translateProvider.translations('en', translationsEN);

		$translateProvider.translations('nl', translationsNL);

		$translateProvider.preferredLanguage('nl');
	});

})();