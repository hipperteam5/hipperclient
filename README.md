## HipperClient
---
## Even een algemene opmerking die niet onaardig bedoeld is maar misschien wel zo overkomt:

Er is een bepaalde structuur bij de clientside. Houd je svp aan die structuur. Het valt mij op (en irriteert me ook) dat het ÉÉN grote teringzooi is geworden. Een patientprofiel heeft bv niet zn eigen profile-controllers nodig, daar kan gewoon een controller voor worden geschreven in de patient-controllers (lijkt mij niet heel raar?). De route /createExercises kan ook zijn /exercises/create. Indelen op entiteit, niet op relatie of iets anders dat in je opkomt en dat in een wereld zonder enige structuur 'ook eventueel wel zou kunnen.'

---